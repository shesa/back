<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('avatar')->nullable();
            $table->foreignId('role_id')->constrained('roles', 'id');
            $table->string('email')->unique();
            $table->string('phone')->unique();
            $table->string('dob')->nullable();
            $table->string('occupation')->nullable();
            $table->string('skills')->nullable();
            $table->string('cv')->nullable();
            $table->enum('marital_status', ['single', 'engaged', 'married'])->default('single');
            $table->string('spouse_name')->nullable();
            $table->string('password');
            $table->timestamp('email_verified_at')->nullable();
            $table->date('registered_on')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
