<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExpendituresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expenditures', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id')->nullable()->constrained('events', 'id');
            $table->string('other_event')->nullable();
            $table->foreignId('recipient_id')->nullable()->constrained('users', 'id');
            $table->string('other_recipient')->nullable();
            $table->double('amount');
            $table->enum('status', ['pending', 'paid', 'cancelled']);
            $table->date('disbursement_date')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expenditures');
    }
}
