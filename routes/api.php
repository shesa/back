<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ContributionController;
use App\Http\Controllers\API\EventController;
use App\Http\Controllers\API\ExpenditureController;
use App\Http\Controllers\API\RoleController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register', [AuthController::class, 'register'])
    ->name('register');

Route::post('/login', [AuthController::class, 'login'])
    ->name('login');

Route::post('/logout', [AuthController::class, 'logout'])
    ->middleware('auth:sanctum');

Route::apiResource('/roles', RoleController::class)
    ->middleware('auth:sanctum');

Route::apiResource('/users', UserController::class)
    ->middleware('auth:sanctum');
Route::post('/users/{id}', [UserController::class, 'update'])
    ->middleware('auth:sanctum');

Route::apiResource('/events', EventController::class)
    ->middleware('auth:sanctum');

Route::apiResource('/contributions', ContributionController::class)
    ->middleware('auth:sanctum');

Route::post('/update-password', [UserController::class, 'updatePassword'])
    ->middleware('auth:sanctum');

Route::apiResource('/expenditures', ExpenditureController::class)
    ->middleware('auth:sanctum');

Route::get('event-expenditure/{id}', [ExpenditureController::class, 'getEventExpenditure'])
    ->middleware('auth:sanctum');
