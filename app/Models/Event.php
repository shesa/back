<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'type',
        'name',
        'description',
        'location',
        'start_date',
        'end_date'
    ];

    /**
     * The attributes that should be populated with the object.
     *
     * @var array
     */
    public $with = [
        'user',
        'contributions'
    ];

    /**
     * Get the event's user
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the event's contributions
     */
    public function contributions()
    {
        return $this->hasMany(Contribution::class);
    }
}
