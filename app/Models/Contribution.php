<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contribution extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'event_id',
        'other_user',
        'amount'
    ];

    /**
     * The attributes that should be populated with the object.
     *
     * @var array
     */
    public $with = [
        'user'
    ];

    /**
     * Get the user contributing
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the event for which the contribution was made
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
