<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Expenditure extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id',
        'other_event',
        'recipient_id',
        'other_recipient',
        'amount',
        'status',
        'disbursement_date'
    ];

    /**
     * The attributes that should be populated with the object.
     *
     * @var array
     */
    public $with = [
        'event',
        'recipient'
    ];

    /**
     * Get the recipient of the expenditure
     */
    public function recipient()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the event for which the expenditure was made
     */
    public function event()
    {
        return $this->belongsTo(Event::class);
    }
}
