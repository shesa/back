<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'middle_name',
        'last_name',
        'avatar',
        'role_id',
        'email',
        'phone',
        'dob',
        'occupation',
        'skills',
        'cv',
        'marital_status',
        'spouse_name',
        'password',
        'registered_on'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'registered_on' => 'date'
    ];

    /**
     * The attributes that should be populated with the object.
     *
     * @var array
     */
    public $with = [
        'role'
    ];

    /**
     * Get the user's role
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Get the user's events
     */
    public function events()
    {
        return $this->hasMany(Event::class);
    }

    /**
     * Get the user's contributions
     */
    public function contributions()
    {
        return $this->hasMany(Contribution::class);
    }
}
