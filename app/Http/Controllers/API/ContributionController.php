<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Contribution;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class ContributionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        try {
            $contributions = Contribution::get();

            Log::info('Contributions searched', ['contributions' => $contributions]);

            return response()->json($contributions);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for contributions', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'user_id' => ['nullable', 'numeric', 'min:1'],
                'event_id' => ['required', 'numeric', 'min:1'],
                'other_user' => ['nullable', 'string', 'max:255'],
                'amount' => ['required', 'numeric']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving contribution validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $userId = $request->user_id;
            $eventId = $request->event_id;
            $otherUser = $request->other_user;
            $amount = $request->amount;

            // Populate a contribution model
            $contribution = new Contribution([
                'user_id' => $userId,
                'event_id' => $eventId,
                'other_user' => $otherUser,
                'amount' => $amount
            ]);

            // Save the contribution and return a message
            if ($contribution->save()) {
                Log::info('Contribution saved successfully', [
                    'contribution_id' => $contribution->id,
                    'user' => auth()->user()
                ]);

                return response()->json([
                    'message' => __('Contribution saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the contribution');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the contribution')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the contribution', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving contribution by ID', ['contribution_id' => $id]);

            return response()->json(Contribution::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve contribution by ID', [
                'contribution_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'user_id' => ['nullable', 'numeric', 'min:1'],
                'event_id' => ['required', 'numeric', 'min:1'],
                'other_user' => ['nullable', 'string', 'max:255'],
                'amount' => ['required', 'numeric']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating contribution validation request failed.', [
                    'contribution_id' => $id,
                    'errors' => $validator->errors()
                ]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $userId = $request->user_id;
            $eventId = $request->event_id;
            $otherUser = $request->other_user;
            $amount = $request->amount;

            // Update role
            $contribution = Contribution::find($id);

            $contribution->user_id = $userId;
            $contribution->event_id = $eventId;
            $contribution->other_user = $otherUser;
            $contribution->amount = $amount;

            if ($contribution->save()) {
                Log::info('Contribution updated successfully.', [
                    'contribution_id' => $id,
                    'user' => auth()->user()
                ]);

                return response()->json($contribution);
            }

            Log::error('An error occurred when updating the contribution');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the contribution')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the contribution', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the contribution
            $contribution = Contribution::find($id);

            // Delete the contribution
            $contribution->delete();

            if ($contribution->trashed()) {
                Log::info('Contribution deleted successfully', [
                    'contribution_id' => $id,
                    'user' => auth()->user()
                ]);

                return response()->json([
                    'message' => __('Contribution deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the contribution');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the contribution')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the contribution', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
