<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Event;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $name = $request->query('name');

            $events = Event::where(function ($query) use ($name) {
                if ($name != null) {
                    return $query->where('name', 'LIKE', '%' . $name . '%')
                        ->orWhere('type', 'LIKE', '%' . $name . '%');
                }
            })
            ->orderBy('start_date', 'desc')
            ->orderBy('name', 'asc')
            ->get();

            Log::info('Events searched', [
                'events' => $events,
                'searched_name' => $name
            ]);

            return response()->json($events);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for events', [
                'error' => $e,
                'searched_name' => $name
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'user_id' => ['nullable', 'numeric', 'min:1'],
                'type' => ['required', Rule::in(['wedding', 'bornhouse', 'bereavement', 'death', 'reunion', 'other'])],
                'name' => ['required', 'string', 'max:255'],
                'description' => ['nullable', 'string'],
                'location' => ['required', 'string', 'max:255'],
                'start_date' => ['required', 'string', 'max:255'],
                'end_date' => ['required', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving event validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $userId = $request->user_id;
            $type = $request->type;
            $name = $request->name;
            $description = $request->description;
            $location = $request->location;
            $startDate = $request->start_date;
            $endDate = $request->end_date;

            // Populate an event object
            $event = new Event([
                'user_id' => $userId,
                'type' => $type,
                'name' => $name,
                'description' => $description,
                'location' => $location,
                'start_date' => $startDate,
                'end_date' => $endDate
            ]);

            // Save the event and return a message
            if ($event->save()) {
                Log::info('Event saved successfully', [
                    'event_id' => $event->id,
                    'user' => auth()->user()
                ]);

                return response()->json([
                    'message' => __('Event saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the event');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the event')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the event', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving event by ID', ['event_id' => $id]);

            return response()->json(Event::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve event by ID', [
                'event_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'user_id' => ['nullable', 'numeric', 'min:1'],
                'type' => ['required', Rule::in(['wedding', 'bornhouse', 'bereavement', 'death', 'reunion', 'other'])],
                'name' => ['required', 'string', 'max:255'],
                'description' => ['nullable', 'string'],
                'location' => ['required', 'string', 'max:255'],
                'start_date' => ['required', 'string', 'max:255'],
                'end_date' => ['required', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating event validation request failed.', [
                    'event_id' => $id,
                    'errors' => $validator->errors()
                ]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $userId = $request->user_id;
            $type = $request->type;
            $name = $request->name;
            $description = $request->description;
            $location = $request->location;
            $startDate = $request->start_date;
            $endDate = $request->end_date;

            // Update event
            $event = Event::find($id);

            $event->user_id = $userId;
            $event->type = $type;
            $event->name = $name;
            $event->description = $description;
            $event->location = $location;
            $event->start_date = $startDate;
            $event->end_date = $endDate;

            if ($event->save()) {
                Log::info('Event updated successfully.', [
                    'event_id' => $id,
                    'user' => auth()->user()
                ]);

                return response()->json($event);
            }

            Log::error('An error occurred when updating the event');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the event')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the event', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the event
            $event = Event::find($id);

            // Delete the event
            $event->delete();

            if ($event->trashed()) {
                Log::info('Event deleted successfully', [
                    'event_id' => $id,
                    'user' => auth()->user()
                ]);

                return response()->json([
                    'message' => __('Event deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the event');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the event')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the event', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
