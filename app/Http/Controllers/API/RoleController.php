<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $name = $request->query('name');

            $roles = Role::where(function ($query) use ($name) {
                if ($name != null) {
                    return $query->where('name', 'LIKE', '%' . $name . '%');
                }
            })
            ->orderBy('name', 'asc')
            ->get();

            Log::info('Roles searched', [
                'roles' => $roles,
                'searched_name' => $name
            ]);

            return response()->json($roles);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for roles', [
                'error' => $e,
                'searched_name' => $name
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving role validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $name = $request->name;

            // Populate a role object
            $role = new Role([
                'name' => $name
            ]);

            // Save the role and return a message
            if ($role->save()) {
                Log::info('Role saved successfully', ['role_id' => $role->id]);

                return response()->json([
                    'message' => __('Role saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the role');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the role')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the role', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving role by ID', ['role_id' => $id]);

            return response()->json(Role::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve role by ID', [
                'role_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating role validation request failed.', [
                    'role_id' => $id,
                    'errors' => $validator->errors()
                ]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $name = $request->name;

            // Update role
            $role = Role::find($id);

            $role->name = $name;

            if ($role->save()) {
                Log::info('Role updated successfully.', ['role_id' => $id]);

                return response()->json($role);
            }

            Log::error('An error occurred when updating the role');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the role')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the role', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the role
            $role = Role::find($id);

            // Delete the role
            $role->delete();

            if ($role->trashed()) {
                Log::info('Role deleted successfully', ['role_id' => $id]);

                return response()->json([
                    'message' => __('Role deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the role');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the role')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the role', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
