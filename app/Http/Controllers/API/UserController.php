<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Mail\AccountCreated;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $username = $request->query('name');

            $users = User::where(function ($query) use ($username) {
                if ($username != null) {
                    return $query->where('first_name', 'LIKE', '%' . $username . '%')
                        ->orWhere('last_name', 'LIKE', '%' . $username . '%')
                        ->orWhere('middle_name', 'LIKE', '%' . $username . '%');
                }
            })
            ->orderBy('first_name', 'asc')
            ->orderBy('last_name', 'asc')
            ->get();

            Log::info('Users searched', [
                'users' => $users,
                'searched_name' => $username
            ]);

            return response()->json($users);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for users', [
                'error' => $e,
                'searched_name' => $username
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'first_name' => ['nullable', 'string', 'max:255'],
                'middle_name' => ['nullable', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'role_id' => ['required', 'numeric', 'min:1'],
                'email' => ['required', 'unique:users', 'email:rfc,dns'],
                'phone' => ['required', 'unique:users', 'string', 'max:25']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving user validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $firstName = $request->first_name;
            $lastName = $request->last_name;
            $middleName = $request->middle_name;
            $roleId = $request->role_id;
            $email = $request->email;
            $phone = $request->phone;
            $password = Str::random(8);

            // Populate a user object
            $user = new User([
                'first_name' => $firstName,
                'middle_name' => $middleName,
                'last_name' => $lastName,
                'email' => $email,
                'role_id' => $roleId,
                'phone' => $phone,
                'password' => Hash::make($password)
            ]);

            // Save the user and return a message
            if ($user->save()) {
                Log::info('User saved successfully', ['user_id' => $user->id]);

                // Send email to user with their password
                Mail::to($email)->send(new AccountCreated($firstName, $password));

                return response()->json([
                    'message' => __('User saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the user');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the user')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the user', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving user by ID', ['user_id' => $id]);

            return response()->json(User::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve user by ID', [
                'user_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'first_name' => ['nullable', 'string', 'max:255'],
                'middle_name' => ['nullable', 'string', 'max:255'],
                'last_name' => ['required', 'string', 'max:255'],
                'avatar' => ['nullable', 'image', 'max:10000'],
                'role_id' => ['required', 'numeric', 'min:1'],
                'email' => ['required', 'email:rfc,dns'],
                'phone' => ['required', 'string', 'max:25'],
                'dob' => ['nullable', 'string', 'max:255'],
                'occupation' => ['nullable', 'string', 'max:255'],
                'skills' => ['nullable', 'string', 'max:255'],
                'cv' => ['nullable', 'file', 'max:10000'],
                'marital_status' => ['nullable', Rule::in(['single', 'engaged', 'married'])],
                'spouse_name' => ['nullable', 'string', 'max:255'],
                'registered_on' => ['nullable', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating user validation request failed.', [
                    'user_id' => $id,
                    'errors' => $validator->errors()
                ]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $firstName = $request->first_name;
            $middleName = $request->middle_name;
            $lastName = $request->last_name;
            $avatar = '';
            if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
                $avatar = $request->avatar->store('users/avatars');
            }
            $roleId = $request->role_id;
            $email = $request->email;
            $phone = $request->phone;
            $dob = $request->dob;
            $occupation = $request->occupation;
            $skills = $request->skills;
            $cv = '';
            if ($request->hasFile('cv') && $request->file('cv')->isValid()) {
                $cv = $request->cv->store('users/cv');
            }
            $maritalStatus = $request->marital_status;
            $spouseName = $request->spouse_name;
            $registeredOn = $request->registered_on;

            // Update user
            $user = User::find($id);

            $user->first_name = $firstName;
            $user->middle_name = $middleName;
            $user->last_name = $lastName;
            $user->avatar = $avatar;
            $user->role_id = $roleId;
            $user->email = $email;
            $user->phone = $phone;
            $user->dob = $dob;
            $user->occupation = $occupation;
            $user->skills = $skills;
            $user->cv = $cv;
            $user->marital_status = $maritalStatus;
            $user->spouse_name = $spouseName;
            $user->registered_on = $registeredOn;

            if ($user->save()) {
                Log::info('User updated successfully.', ['user_id' => $id]);

                return response()->json($user);
            }

            Log::error('An error occurred when updating the user');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the user')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the user', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update user password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'old_password' => ['required', 'string', 'max:255'],
                'new_password' => ['required', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating user password validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $oldPassword = $request->old_password;
            $newPassword = $request->new_password;

            $authUser = auth()->user();

            if (Hash::check($oldPassword, $authUser->password)) {
                $authUser->password = Hash::make($newPassword);

                if ($authUser->save()) {
                    Log::info('User password updated successfully.', ['user_id' => $authUser->id]);

                    return response()->json($authUser);
                }

                Log::error('An error occurred when updating the user password');

                return response()->json(
                    [
                        'message' => __('An error occurred when updating the user password')
                    ],
                    500
                );
            }

            Log::error('The password entered does not match the password saved for this user');

            return response()->json(
                [
                    'message' => __('The password entered does not match the password saved for this user')
                ],
                403
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the user password', [
                'error' => $e,
                'user' => $authUser
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the user
            $user = User::find($id);

            // Delete the user
            $user->delete();

            if ($user->trashed()) {
                $user->forceDelete();

                Log::info('User deleted successfully', ['user_id' => $id]);

                return response()->json([
                    'message' => __('User deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the user');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the user')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the user', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
