<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Expenditure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ExpenditureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        try {
            $status = $request->query('status');

            $expenditures = Expenditure::where(function ($query) use ($status) {
                if ($status != null) {
                    return $query->where('status', $status);
                }
            })
            ->orderBy('updated_at', 'desc')
            ->get();

            Log::info('Expenditures searched', [
                'expenditures' => $expenditures,
                'searched_status' => $status
            ]);

            return response()->json($expenditures);
        } catch (\Throwable $e) {
            Log::error('An error occurred when searching for expenditures', [
                'error' => $e,
                'searched_status' => $status
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'event_id' => ['nullable', 'numeric', 'min:1'],
                'other_event' => ['nullable', 'string', 'max:255'],
                'recipient_id' => ['nullable', 'numeric', 'min:1'],
                'other_recipient' => ['nullable', 'string', 'max:255'],
                'amount' => ['required', 'numeric'],
                'status' => ['required', Rule::in(['pending', 'paid', 'cancelled'])],
                'disbursement_date' => ['nullable', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Saving expenditure validation request failed.', ['errors' => $validator->errors()]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $eventId = $request->event_id;
            $otherEvent = $request->other_event;
            $recipientId = $request->recipient_id;
            $otherRecipient = $request->other_recipient;
            $amount = $request->amount;
            $status = $request->status;
            $disbursementDate = $request->disbursement_date;

            // Populate an Expenditure object
            $expenditure = new Expenditure([
                'event_id' => $eventId,
                'other_event' => $otherEvent,
                'recipient_id' => $recipientId,
                'other_recipient' => $otherRecipient,
                'amount' => $amount,
                'status' => $status,
                'disbursement_date' => $disbursementDate
            ]);

            // Save the expenditure and return a message
            if ($expenditure->save()) {
                Log::info('Expenditure saved successfully', [
                    'expenditure_id' => $expenditure->id,
                    'user' => auth()->user()
                ]);

                return response()->json([
                    'message' => __('Expenditure saved successfully')
                ]);
            }

            Log::error('An error occurred when saving the expenditure');

            return response()->json(
                [
                    'message' => __('An error occurred when saving the expenditure')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to save the expenditure', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            Log::info('Retrieving expenditure by ID', ['expenditure_id' => $id]);

            return response()->json(Expenditure::find($id));
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve expenditure by ID', [
                'expenditure_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Get the expenditure for an event.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEventExpenditure($id)
    {
        try {
            Log::info('Retrieving expenditure by event ID', ['event_id' => $id]);

            $expenditure = Expenditure::where('event_id', $id)->first();

            return response()->json($expenditure);
        } catch (\Throwable $e) {
            Log::error('Failed to retrieve expenditure by event ID', [
                'event_id' => $id,
                'error' => $e
            ]);

            return response()->json($e, 500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            // Validate sent data
            $validator = Validator::make($request->all(), [
                'event_id' => ['nullable', 'numeric', 'min:1'],
                'other_event' => ['nullable', 'string', 'max:255'],
                'recipient_id' => ['nullable', 'numeric', 'min:1'],
                'other_recipient' => ['nullable', 'string', 'max:255'],
                'amount' => ['required', 'numeric'],
                'status' => ['required', Rule::in(['pending', 'paid', 'cancelled'])],
                'disbursement_date' => ['nullable', 'string', 'max:255']
            ]);

            if ($validator->fails()) {
                Log::warning('Updating expenditure validation request failed.', [
                    'expenditure_id' => $id,
                    'errors' => $validator->errors()
                ]);

                return response()->json($validator->errors(), 422);
            }

            // Retrieve validated data
            $eventId = $request->event_id;
            $otherEvent = $request->other_event;
            $recipientId = $request->recipient_id;
            $otherRecipient = $request->other_recipient;
            $amount = $request->amount;
            $status = $request->status;
            $disbursementDate = $request->disbursement_date;

            // Update expenditure
            $expenditure = Expenditure::find($id);

            $expenditure->event_id = $eventId;
            $expenditure->other_event = $otherEvent;
            $expenditure->recipient_id = $recipientId;
            $expenditure->other_recipient = $otherRecipient;
            $expenditure->amount = $amount;
            $expenditure->status = $status;
            $expenditure->disbursement_date = $disbursementDate;

            if ($expenditure->save()) {
                Log::info('Expenditure updated successfully.', [
                    'expenditure_id' => $id,
                    'user' => auth()->user()
                ]);

                return response()->json($expenditure);
            }

            Log::error('An error occurred when updating the expenditure');

            return response()->json(
                [
                    'message' => __('An error occurred when updating the expenditure')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to update the expenditure', ['error' => $e]);

            return response()->json($e, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            // Retrieve the expenditure
            $expenditure = Expenditure::find($id);

            // Delete the expenditure
            $expenditure->delete();

            if ($expenditure->trashed()) {
                Log::info('Expenditure deleted successfully', [
                    'expenditure_id' => $id,
                    'user' => auth()->user()
                ]);

                return response()->json([
                    'message' => __('Expenditure deleted successfully')
                ]);
            }

            Log::error('An error occurred when deleting the expenditure');

            return response()->json(
                [
                    'message' => __('An error occurred when deleting the expenditure')
                ],
                500
            );
        } catch (\Throwable $e) {
            Log::error('An error occurred when trying to delete the expenditure', ['error' => $e]);

            return response()->json($e, 500);
        }
    }
}
